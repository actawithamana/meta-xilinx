LINUX_VERSION = "3.17"
# This points at the 'xilinx-v2014.4' tag
SRCREV ?= "7b042ef9ea5cc359a22110c75342f8e28c9cdff1"

include linux-xlnx.inc

COMPATIBLE_MACHINE_zynqmp = "zynqmp"

